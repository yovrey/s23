let trainer = {
	name:"Ash Kethum",
	age: 10,
	pokemon: ["Pickachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: 
		{
		hoenn: ["May", "Max"],
		kanto: ["Brocky", "Misty"]
		},
	talk: function(){
		console.log("Pickachu! I choose you!")
	}
}
console.log("Result of dot notation: ");
console.log(trainer.name);

console.log("Result of square bracket notaiton: ");
console.log(trainer['pokemon']);

console.log("Result of talk method ");
trainer.talk();

// --------------------------------------------------------


let pikachu= new Pokemon("Pikachu", 12);
let geodude= new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);


function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	this.tackle = function(target){
		let preyHealth = target.health - this.attack;
		let predatorHealth = this.health - this.attack;
		console.log( this.name + " tackled " + target.name);

		if (preyHealth <= 0 && predatorHealth >= 1) {
		console.log(target.name +"'s health is now reduced to " + preyHealth );
		console.log( target.name + " has fainted ");
		}
	

		if (preyHealth >= 1 && predatorHealth <= 0) {
		console.log(target.name + " won. ");
		console.log(this.name + "'s health is now reduced to " + predatorHealth );
		console.log( target.name + " is still alive.");
		}

		if (preyHealth >= 1 && predatorHealth>= 1) {
		console.log(target.name + "'s health is now reduced to " + predatorHealth );
		console.log( target.name + " is still alive.");
		}

		if ( preyHealth <= 0 && predatorHealth <= 0){
		console.log(" It's a draw.");
		}
	
	
	};


}



geodude.tackle(pikachu);
mewtwo.tackle(geodude);

